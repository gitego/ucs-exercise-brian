package it.codeland.core.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NavLinks {
    private static final Logger logger = LoggerFactory.getLogger(NavLinks.class);

    @SlingObject
    private Resource currentResource;

    public List<Map<String, String>> getLinks() {
        List<Map<String, String>> links = new ArrayList<>();

        try {
            Resource linkDetails = currentResource.getChild("linkwithdetails");
            if (linkDetails == null) {
                return Collections.emptyList();
            } else {
                for (Resource child : linkDetails.getChildren()) {
                    Map<String, String> linksMap = new HashMap<>();
                    linksMap.put("linklabel", child.getValueMap().get("linklabel", String.class));
                    linksMap.put("icon", child.getValueMap().get("icon", String.class));
                    linksMap.put("link", child.getValueMap().get("link", String.class));
                    linksMap.put("target", child.getValueMap().get("target", String.class));
                    links.add(linksMap);
                }
            }
        } catch (Exception e) {
            logger.info("\n Error {} ", e.getMessage());
        }
        return links;
    }

}