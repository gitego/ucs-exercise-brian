package it.codeland.core.models;

import com.google.common.collect.Iterables;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SliderModel {
    private final List<Resource> slides = new ArrayList<>();
    private final List<Resource> orderedSlides = new ArrayList<>();

    @SlingObject
    private Resource currentResource;

    @Inject
    private String[] slideOrder;

    @PostConstruct
    protected void init() throws PersistenceException {

        Iterator<Resource> children = currentResource.listChildren();
        if (children == null) {
            return;
        }

        Resource currentChild;
        while (children.hasNext()) {
            currentChild = children.next();
            slides.add(currentChild);
        }

        if (slideOrder != null) {
            for (String slideName : slideOrder) {
                Resource activeSlide = slides.stream().filter(item -> {
                    if (item.getName().equals(slideName)) {
                        return true;
                    }
                    return false;
                }).findAny().orElse(null);

                if (activeSlide != null) {
                    orderedSlides.add(activeSlide);
                }
            }
        }
    }

    public List<Resource> getSlides() {
        return orderedSlides;
    }
}
